from rest_framework import serializers
from router.models import *
from django.shortcuts import get_object_or_404
from router.token import get_access_token

class UserSignUpViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'