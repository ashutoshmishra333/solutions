from router import views
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

router = DefaultRouter()
urlpatterns = router.urls
from .views import*
urlpatterns += [
    path('v1/user-sign-up/', UserSignUpView.as_view({'post':'create'})),
    path('v1/log-in/', UserLoginView.as_view({'post':'create'})),
    path('v1/address-update/<int:pk>/', UserSignUpView.as_view({'put':'update'})),
    

]
