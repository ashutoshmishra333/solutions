from django.shortcuts import render

from django.utils import timezone
from rest_framework import status
from rest_framework import viewsets
from router.models import *
from rest_framework.views import APIView
from router.serializers import *
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from router.token import get_access_token
import traceback
import sys
from router.token import*
from django.contrib.auth import authenticate

class UserSignUpView(viewsets.ViewSet):
    def create(self, request):
        try:
            email=request.data.get('email')
            password=request.data.get('password')
            address=request.data.get('address')
            email_exist=Account.objects.filter(email=email)
            if email_exist:
                raise Exception('email id already exist')
            account_obj=Account()
            account_obj.email=email
            account_obj.address=address
            account_obj.set_password(password)
            account_obj.save()
            Application.objects.get_or_create(user=account_obj, client_type=Application.CLIENT_CONFIDENTIAL,
												authorization_grant_type=Application.GRANT_PASSWORD)
            token=get_access_token(account_obj)
            return Response({"token":token,"user_id":account_obj.id, "success": True}, status=status.HTTP_200_OK)
        except Exception as e:
            print("Exception",e)
            return Response({"message": str(e),"success":False,},status=status.HTTP_200_OK)
    
    def update(self, request,pk=None):
        try:
            instance=Account.objects.get(pk=pk)
            serializer = UserSignUpViewSerializer(instance,data=request.data,partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        except Exception as e:
            print("Exception",e)
            return Response({"message": str(e),"success":False,},status=status.HTTP_200_OK)

class UserLoginView(viewsets.ViewSet):
    def create(self, request):
        try:
            data=request.data
            email=data.get('email')
            password=data.get('password')
            if not all([email,password]):
                raise Exception("all fields are mandatory")
            email_exist=Account.objects.filter().count()
            if not email_exist:
                raise Exception("Account with this email does not exist!")
            else:
                account=authenticate(username=email,password=password)
            if account is not None:
                token=get_access_token(account)
                serializer = UserSignUpViewSerializer(instance = account)
                return Response({"message": serializer.data,"success":True},status=status.HTTP_200_OK)
            else:
                raise Exception("Credentials not matched")
        except Exception as e:
            print("Exception",e)
            return Response({"message": str(e),"success":False,},status=status.HTTP_200_OK)

