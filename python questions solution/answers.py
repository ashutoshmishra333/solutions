

Q1=Write a function which returns sum of the list of numbers

def listsum(list):
    total=0
    for data in list:
        total += data
    return total

# The test case:
print(listsum([1,2,3,4,5,6,7,8,9,10]))
##################################################end of 1st solution###########################
Q2=Write a function in python in python, which will return maximum i.e function should return dictionary like
{
  “3” : 70
}
ans:
import collections
def largest(data):
    import collections
    data =collections.Counter(data)
    return dict(data.most_common(1))
print(largest({'a':1000, 'b':3000, 'c': 100222}))

###############################################END OF SOLUTION OF QUESTION 2#####################################

Q3=Write a function in python in python, which will return maximum
sol:
def getMaxLength(arr, n): 
  
    # intitialize count 
    count = 0 
      
    # initialize max 
    result = 0 
  
    for i in range(0, n): 
      
        # Reset count when 0 is found 
        if (arr[i] == 0): 
            count = 0
  
        # If 1 is found, increment count 
        # and update result if count  
        # becomes more. 
        else: 
              
            # increase count 
            count+= 1 
            result = max(result, count)  
          
    return result  
  
# Driver code 
arr = [1, 1, 0, 0, 1, 0, 1,  
             0, 1, 1, 1, 1,1]  
n = len(arr)  
  
print(getMaxLength(arr, n))

############################################ END OF SOLUTION OF QUESTION 3##############################





